const cardsTitle = document.querySelectorAll("[data-card-info=card__title]");
const cardsDescription = document.querySelectorAll("[data-card-info=card__description]");

const mapper = (list, mutator) => {
  list.forEach(mutator);
};

// Задание 1

const textToUpper = (element) => {
    element.textContent = element.textContent.toUpperCase();
};


// Задание 2

const textSlice = (element) => {

    const content = element.textContent;

    if (content.length > 20) {
      element.textContent = content.slice(0, 20) + "...";
    }
};


mapper(cardsTitle, textToUpper);

mapper(cardsDescription, textSlice);