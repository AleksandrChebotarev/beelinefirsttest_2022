const checkboxLectures = document.getElementById("check-lectures");
const submenu = document.querySelector(".submenu");
const btnCloseMenu = document.querySelector(".btn-close-menu");
const layoutLeft = document.querySelector(".layout__left");
const btnOpenMenu = document.querySelector(".header-topbar__btn");
const layout = document.querySelector(".layout");
const layoutBody = document.querySelector(".layout__body");
const sliderWrapper = document.querySelector(".section-wrapper__main");
const listLecturesBeginer = document.querySelector(".lectures-beginer");
const items = document.querySelectorAll(".item-beginer");
const buttonPrev = document.querySelector(".button-prev");
const buttonNext = document.querySelector(".button-next");


// ЗАДАНИЕ *

const isMobile = {
  Android: function () {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function () {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function () {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function () {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function () {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: function () {
    return (
      isMobile.Android() ||
      isMobile.BlackBerry() ||
      isMobile.iOS() ||
      isMobile.Opera() ||
      isMobile.Windows()
    );
  },
};

// Скроллы для мобилки

let init = () => {
  if (isMobile.any()) {
    layout.classList.add("fullHeight");

    layoutBody.classList.add("layout__body_overflow-y_scroll");
  } else {
    if (layout.classList.contains("fullHeight")) {
      layout.classList.remove("fullHeight");
    }

    if (layoutBody.classList.contains("layout__body_overflow-y_scroll")) {
      layoutBody.classList.remove("layout__body_overflow-y_scroll");
    }
  }
};

init();

window.addEventListener("resize", init);

// Меню выпадающе Aside

checkboxLectures.addEventListener("click", (event) =>
  event.target.checked
    ? submenu.classList.add("open")
    : submenu.classList.contains("open")
    ? submenu.classList.remove("open")
    : null
);

// toggle

btnOpenMenu.addEventListener("click", () => {
  layoutLeft.classList.add("open");
});

btnCloseMenu.addEventListener("click", () => {
  layoutLeft.classList.remove("open");
});

// ЗАДАНИЕ **

let count = 0;                                              // счётчик
let itemWidth = 266;                                        // карточка с учетом margin
let widthSliderWrapper = sliderWrapper.offsetWidth;         // ширина видимого контента списка

// Кол-во сдвигов

let amount = Math.ceil(
  (items.length * itemWidth - 50 - widthSliderWrapper) / itemWidth
);

// Сдвиг списка

let rollSlider = (item) =>
  (item.style.transform = "translate(-" + itemWidth * count + "px)");

// Свайпер

let swiper = (prev, next, list) => {
  next.addEventListener("click", function () {
    if (count <= amount - 1 && count >= 0) {
      count++;
      rollSlider(list);
    } else {
      count = amount;
    }
  });
  prev.addEventListener("click", function () {
    if (count <= amount && count > 0) {
      count--;
      rollSlider(list);
    } else {
      count = 0;
    }
  });
};

swiper(buttonPrev, buttonNext, listLecturesBeginer);